﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogApp.Areas.admin.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Lütfen kullanıcı adını giriniz.")]
        [Display(Name = "Kullanıcı Adı")]
        public string kullaniciAdi { get; set; }

        [Required(ErrorMessage = "Lütfen şifrenizi giriniz.")]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre")]
        public string Password { get; set; }
    }
}