﻿using BlogApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            BlogContext db = new BlogContext();

            List<Post> postList = db.Posts.OrderByDescending(i => i.Tarih).Take(5).ToList();

            return View(postList);
        }

        public ActionResult kategoriGetir()
        {
            BlogContext db = new BlogContext();

            List<Category> categoryList = db.Categories.Take(5).ToList();

            return PartialView(categoryList);
        }

    }
}