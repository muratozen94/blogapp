﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogApp.Models
{
    public class Category
    {
        //KAtegori Tablosu
        public int id { get; set; }
        public string Ad { get; set; }

        //İlişkiler
        public virtual Post Post{ get; set; }
    }
}