﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogApp.Models
{
    public class Comment
    {

        //Yorum tablosu
        public int id { get; set; }
        public DateTime Tarih { get; set; }
        public string Icerik { get; set; }
        public bool Onay { get; set; }

        // İlişkiler
        public virtual Post Post { get; set; }
        public virtual User User { get; set; }
    }
}