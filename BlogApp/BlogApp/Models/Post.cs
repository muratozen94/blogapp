﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogApp.Models
{
    public class Post
    {
        [Key]
        public int id { get; set; }
        public string Baslik { get; set; }
        public string Ozet { get; set; }
        public string Icerik { get; set; }

        public string ResimYol { get; set; }
        public DateTime Tarih { get; set; }

        public virtual User User { get; set; }

        public virtual List<Comment> Comments{ get; set; }
        public virtual List<Tags> Tags { get; set; }
    }
}