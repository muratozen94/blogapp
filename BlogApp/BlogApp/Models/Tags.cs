﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogApp.Models
{
    public class Tags
    {
        public int TagsID { get; set; }

        public string Name { get; set; }

        //İlişkiler
        public virtual List<Post> Posts { get; set; }
    }
}