﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogApp.Models
{
    public class User
    {
        public int ID { get; set; }

        public string Ad { get; set; }

        public string Soyad { get; set; }

        public string Eposta { get; set; }

        public string ResimYol { get; set; }

        public DateTime UyeTarih { get; set; }

        //İlişkiler
        public virtual List<Comment> Comments { get; set; }
        public virtual List<Post> Posts { get; set; }
    }
}